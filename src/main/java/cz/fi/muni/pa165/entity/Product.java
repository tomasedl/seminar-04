package cz.fi.muni.pa165.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "ESHOP_PRODUCTS")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //proc to pada se sequence???

    @Column(nullable = false, unique = true)
    private String name;

    @Column(columnDefinition = "DATE")
    private java.time.LocalDate addedDate;

    @Enumerated(EnumType.STRING)
    private Color color; //pozor: nesmime prejmenovat prvky v enumu - lepsi reseni je https://www.baeldung.com/jpa-persisting-enums-in-jpa#callbacks

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    } //todo: kontrola na null?

    public Color getColor() {
        return color;
    }

    public LocalDate getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDate addedDate) {
        this.addedDate = addedDate;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || ! (o instanceof Product)) return false;
        Product product = (Product) o;

        return getName().equals(product.getName()) && getColor() == product.getColor();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getColor());
    }
}
